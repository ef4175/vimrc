"Indentation
set tabstop=2
set shiftwidth=2
set expandtab
set autoindent

"Wrapping
set nowrap

"Backspace behavior
set nocompatible
set backspace=2

"Syntax highlighting
syntax on

"Color column
highlight ColorColumn ctermbg=0
set colorcolumn=79

"Unsaved buffer switching
set hidden
